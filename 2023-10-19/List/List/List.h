#pragma once
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

//定义双向链表节点的结构
typedef int LTDataType;

typedef struct ListNode {
	LTDataType data;//val == data
	struct ListNode* next;
	struct ListNode* prev;
}LTNode;

void LTPrint(LTNode* phead);

//链表的初始化以及销毁
//void LTInit(LTNode** pphead);//前提：我们要传入一个头结点
LTNode* LTInit();//不需要传入参数，调用该方法之后给我们返回一个头结点

//实际这里更倾向于传递一级指针，因为要保持接口一致性
void LTDestroy(LTNode* phead);
//void LTDestroy(LTNode** pphead);


//在双向链表中不会改变哨兵位，所以这里都可以传一级指针
//插入删除操作
void LTPushBack(LTNode* phead,LTDataType x);
void LTPushFront(LTNode* phead,LTDataType x);
void LTPopBack(LTNode* phead);
void LTPopFront(LTNode* phead);

//在pos位置之后插入数据
void LTInsert(LTNode* pos, LTDataType x);
void LTErase(LTNode* pos);
LTNode* LTFind(LTNode* phead, LTDataType x);

