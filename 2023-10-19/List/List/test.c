#include"List.h"

void ListTest() {
	//LTNode* plist = NULL;
	//LTInit(&plist);
	LTNode* plist = LTInit();
	LTPushBack(plist, 1);
	LTPushBack(plist, 2);
	LTPushBack(plist, 3);
	LTPushBack(plist, 4);
	LTPrint(plist);// 1 2 3 4

	//LTPushFront(plist, 5);
	//LTPushFront(plist, 6);
	//LTPushFront(plist, 7);
	//LTPrint(plist);//7 6 5 1 2 3 4

	//LTPopBack(plist);
	//LTPopBack(plist);
	//LTPopBack(plist);
	//LTPopBack(plist);
	//LTPopBack(plist);
	//LTPopFront(plist);
	//LTPopFront(plist);
	//LTPopFront(plist);
	//LTPopFront(plist);
	//LTPopFront(plist);

	////测试指定位置之后插入
	//LTNode* find = LTFind(plist, 1);
	////LTInsert(find, 11);
	//LTErase(find);
	//LTPrint(plist);
	//LTDestroy(&plist);
	LTDestroy(plist);
	//传一级指针的时候需要手动将plist置为空
	plist = NULL;
}

int main()
{
	ListTest();
	return 0;
}
