#include"List.h"
//前提：我们要传入一个头结点
//void LTInit(LTNode** pphead) {
//	*pphead = (LTNode*)malloc(sizeof(LTNode));
//	if (*pphead == NULL) {
//		perror("malloc error");
//		return;
//	}
//	//节点有三部分内容：数据 前驱指针 后继指针
//	(*pphead)->data = -1;//哨兵位
//	(*pphead)->next = (*pphead)->prev = *pphead;
//}
//不需要传入参数，调用该方法之后给我们返回一个头结点
LTNode* LTInit() {
	LTNode* phead = (LTNode*)malloc(sizeof(LTNode));
	if (phead == NULL) {
		perror("malloc error");
		return;
	}
	phead->data = -1;
	phead->next = phead->prev = phead;
	return phead;
}
LTNode* ListBuyNode(LTDataType x) {
	LTNode* node = (LTNode*)malloc(sizeof(LTNode));
	node->data = x;
	node->next = node->prev = NULL;
	return node;
}
//插入操作
void LTPushBack(LTNode* phead, LTDataType x) {
	assert(phead);
	LTNode* node = ListBuyNode(x);

	//先处理新节点node的前驱和后继指针
	node->prev = phead->prev;
	node->next = phead;

	//再处理phead->prev(之前的尾结点) 和  phead
	phead->prev->next = node;
	phead->prev = node;
}
void LTPrint(LTNode* phead) {
	LTNode* cur = phead->next;

	while (cur != phead)
	{
		printf("%d->", cur->data);
		cur = cur->next;
	}
	printf("\n");
}
void LTPushFront(LTNode* phead, LTDataType x) {
	assert(phead);
	LTNode* node = ListBuyNode(x);
	//node节点的 prev next
	node->prev = phead;
	node->next = phead->next;

	//处理phead  phead->next
	phead->next->prev = node;
	phead->next = node;
}
void LTPopBack(LTNode* phead) {
	assert(phead);
	//链表不能为空链表：链表中只有一个哨兵位节点
	assert(phead->next != phead);
	
	LTNode* del = phead->prev;
	//先处理del的prev节点
	del->prev->next = phead;
	//处理phead
	phead->prev = del->prev;
	
	free(del);
	del = NULL;
}
void LTPopFront(LTNode* phead) {
	assert(phead && phead->next != phead);
	LTNode* del = phead->next;

	//phead del->next
	del->next->prev = phead;
	phead->next = del->next;
	free(del);
	del = NULL;
}
//在pos位置之后插入数据
void LTInsert(LTNode* pos, LTDataType x) {
	assert(pos);
	LTNode* node = ListBuyNode(x);
	//node的prev 和 next
	node->next = pos->next;
	node->prev = pos;

	//pos的next 和 pos->next的prev
	pos->next = node;
	node->next->prev = node;
}
void LTErase(LTNode* pos) {
	assert(pos);
	//pos->prev:next  pos  pos->next:prev
	pos->next->prev = pos->prev;
	pos->prev->next = pos->next;
	free(pos);
	pos = NULL;
}
LTNode* LTFind(LTNode* phead, LTDataType x) {
	assert(phead);
	LTNode* cur = phead->next;
	while ( cur != phead)
	{
		if (cur->data == x) {
			return cur;
		}
		cur = cur->next;
	}
	return NULL;
}
void LTDestroy(LTNode* phead) {
	assert(phead);
	LTNode* cur = phead->next;
	while (cur != phead)
	{
		LTNode* next = cur->next;
		free(cur);
		cur = next;
	}
	//除了循环之后，哨兵位还没有释放
	free(phead);
	phead = NULL;
}
//void LTDestroy(LTNode** pphead) {
//	assert(pphead && *pphead);
//	LTNode* cur = (*pphead)->next;
//	while ( cur!=*pphead )
//	{
//		LTNode* next = cur->next;
//		free(cur);
//		cur = next;
//	}
//	free(*pphead);
//	*pphead = NULL;
//}
