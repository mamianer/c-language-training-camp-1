#include"SeqList.h"
#include"Contact.h"

//void SLtest() {
//	SL sl;
//	SLInit(&sl);
//	//顺序表的具体操作
//	//SLPushBack(&sl, 1);
//	//SLPushBack(&sl, 2);
//	//SLPushBack(&sl, 3);
//	//SLPushBack(&sl, 4);//1 2 3 4
//	//SLPrint(&sl);
//	////头插
//	//SLPushFront(&sl, 5);//5 1 2 3 4
//	//SLPushFront(&sl, 6);//6 5 1 2 3 4
//	//SLPushFront(&sl, 7);//7 6 5 1 2 3 4
//	//SLPrint(&sl);
//	//尾删
//	SLPopBack(&sl);
//	SLPrint(&sl);
//	SLPopBack(&sl);
//	SLPrint(&sl);
//	SLDestroy(&sl);
//}
//
//void SLtest02() {
//	SL sl;
//	SLInit(&sl);
//	SLPushBack(&sl, 1);
//	SLPushBack(&sl, 2);
//	SLPushBack(&sl, 3);
//	SLPushBack(&sl, 4);
//	SLPrint(&sl);
//
//	//头删
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//	////到这里顺序表已经没有有效的数据了
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//
//	//在指定位置之前插入数据
//	//SLInsert(&sl, sl.size, 11);
//	//SLPrint(&sl);
//
//	//删除指定位置的数据
//	//SLErase(&sl, 100);
//	//SLPrint(&sl);
//
//	//bool findRet = SLFind(&sl, 33);
//	//if (findRet) {
//	//	printf("找到了！\n");
//	//}
//	//else {
//	//	printf("没有找到！\n");
//	//}
//	//SLDestroy(&sl);
//}

void contact01() {
	contact con;
	ContactInit(&con);
	//往通讯录中插入数据
	ContactAdd(&con);
	ContactAdd(&con);
	ContactShow(&con);

	ContactDel(&con);
	ContactShow(&con);
	ContactDestroy(&con);
}

int main() {
	//SLtest();
	//SLtest02();
	contact01();
	return 0;
}