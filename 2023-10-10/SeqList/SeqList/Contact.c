#include"Contact.h"
#include"SeqList.h"

void ContactInit(contact* pcon) {
	SLInit(pcon);
}
void ContactDestroy(contact* pcon) {
	SLDestroy(pcon);
}

//添加联系人
void ContactAdd(contact* pcon) {
	//接下来要获取的信息都是CInfo结构体里要求的数据
	CInfo info;
	printf("请输入联系人姓名：\n");
	scanf("%s", info.name);
	printf("请输入联系人的性别：\n");
	scanf("%s", info.sex);
	printf("请输入联系人的年龄：\n");
	scanf("%d", &info.age);
	printf("请输入联系人的电话：\n");
	scanf("%s", info.tel);
	printf("请输入联系人的住址：\n");
	scanf("%s", info.addr);

	//联系人数据都获取到了，并保存到了结构体info中
	//往通讯录（顺序表）中插入数据
	SLPushBack(pcon, info);
}

int FindByName(contact* pcon,char name[]) {
	for (int i = 0; i < pcon->size; i++)
	{
		if (strcmp(pcon->a[i].name, name) == 0) {
			return i;
		}
	}
	return -1;
}

//删除联系人
void ContactDel(contact* pcon) {
	//直接强制要求用户使用联系人名称来查找
	printf("请输入要删除的用户名称：\n");
	char name[NAME_MAX];
	scanf("%s", name);
	int findidex = FindByName(pcon, name);
	if (findidex < 0) {
		printf("要删除的联系人不存在！\n");
		return;
	}
	//找到了，要删除findidx位置的数据
	SLErase(pcon, findidex);
}
//修改联系人
void ContactModify(contact* pcon);
//查看通讯录
void ContactShow(contact* pcon) {
	//打印通讯录所有的数据
	//先打印表头文字
	printf("%s %s %s %s %s\n", "姓名", "性别", "年龄", "电话", "住址");
	for (int i = 0; i < pcon->size; i++)
	{
		printf("%-4s %-4s %-4d %-4s %-4s\n",
			pcon->a[i].name,
			pcon->a[i].sex,
			pcon->a[i].age,
			pcon->a[i].tel,
			pcon->a[i].addr
			);
	}
}
//查找指定联系人
void ContactFind(contact* pcon);