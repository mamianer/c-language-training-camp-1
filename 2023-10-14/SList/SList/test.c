#include"SList.h"

void slttest() {
	SLNode* node1 = (SLNode*)malloc(sizeof(SLNode));
	node1->data = 1;
	SLNode* node2 = (SLNode*)malloc(sizeof(SLNode));
	node2->data = 2;
	SLNode* node3 = (SLNode*)malloc(sizeof(SLNode));
	node3->data = 3;
	SLNode* node4 = (SLNode*)malloc(sizeof(SLNode));
	node4->data = 4;

	node1->next = node2;
	node2->next = node3;
	node3->next = node4;
	node4->next = NULL;

	//打印链表
	SLNode* plist = node1;
	SLPrint(plist);
}

void slttest01() {
	SLNode* plist = NULL;
	//尾插
	SLPushBack(&plist, 1);
	SLPushBack(&plist, 2);
	SLPushBack(&plist, 3);
	SLPushBack(&plist, 4);//1->2->3->4->NULL
	SLPrint(plist);
	//SLPushFront(&plist, 1);
	//SLPushFront(&plist, 2);
	//SLPushFront(&plist, 3);
	//SLPushFront(&plist, 4);//4->3->2->1->NULL
	//尾删
	//SLPopBack(&plist);
	//SLPopBack(&plist);
	//SLPopBack(&plist);
	//SLPopBack(&plist);
	//头删
	//SLPopFront(&plist);
	//SLPopFront(&plist);
	//SLPopFront(&plist);
	//SLPopFront(&plist);
	//SLPopFront(&plist);
	//SLNode* find = SLFind(&plist, 4);
	//SLInsert(&plist, find,11);//1->11->2->3->4->NULL
	//在指定位置之后插入数据
	//SLInsertAfter(find, 100);
	//删除pos位置的节点
	//SLErase(&plist, find);//1->2->3->NULL
	//删除pos之后的节点
	//SLEraseAfter(find);
	//销毁链表
	SLDesTroy(&plist);
	SLPrint(plist);
}
int main() {
	//slttest();
	slttest01();
	return 0;
}