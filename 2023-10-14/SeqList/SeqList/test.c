#include"SeqList.h"
#include"Contact.h"

//void SLtest() {
//	SL sl;
//	SLInit(&sl);
//	//顺序表的具体操作
//	//SLPushBack(&sl, 1);
//	//SLPushBack(&sl, 2);
//	//SLPushBack(&sl, 3);
//	//SLPushBack(&sl, 4);//1 2 3 4
//	//SLPrint(&sl);
//	////头插
//	//SLPushFront(&sl, 5);//5 1 2 3 4
//	//SLPushFront(&sl, 6);//6 5 1 2 3 4
//	//SLPushFront(&sl, 7);//7 6 5 1 2 3 4
//	//SLPrint(&sl);
//	//尾删
//	SLPopBack(&sl);
//	SLPrint(&sl);
//	SLPopBack(&sl);
//	SLPrint(&sl);
//	SLDestroy(&sl);
//}
//
//void SLtest02() {
//	SL sl;
//	SLInit(&sl);
//	SLPushBack(&sl, 1);
//	SLPushBack(&sl, 2);
//	SLPushBack(&sl, 3);
//	SLPushBack(&sl, 4);
//	SLPrint(&sl);
//
//	//头删
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//	////到这里顺序表已经没有有效的数据了
//	//SLPopFront(&sl);
//	//SLPrint(&sl);
//
//	//在指定位置之前插入数据
//	//SLInsert(&sl, sl.size, 11);
//	//SLPrint(&sl);
//
//	//删除指定位置的数据
//	//SLErase(&sl, 100);
//	//SLPrint(&sl);
//
//	//bool findRet = SLFind(&sl, 33);
//	//if (findRet) {
//	//	printf("找到了！\n");
//	//}
//	//else {
//	//	printf("没有找到！\n");
//	//}
//	//SLDestroy(&sl);
//}

//void contact01() {
//	contact con;
//	ContactInit(&con);
//	//往通讯录中插入数据
//	ContactAdd(&con);
//	//ContactAdd(&con);
//	ContactShow(&con);
//
//	//ContactDel(&con);
//	//ContactShow(&con);
//	ContactModify(&con);
//	ContactShow(&con);
//	ContactFind(&con);
//
//	ContactDestroy(&con);
//}
//通讯录界面
void menu() {
	printf("****************通讯录********************\n");
	printf("******1、添加联系人    2、删除联系人********\n");
	printf("******3、修改联系人    4、查找联系人********\n");
	printf("******5、查看通讯录    0、 退 出    ********\n");
	printf("****************************************\n");
}
int main() {
	int op = -1;
	//定义一个通讯录
	contact con;
	ContactInit(&con);
	do {
		menu();
		printf("请选择您的操作：\n");
		scanf("%d", &op);
		switch (op)
		{
		case 1:
			ContactAdd(&con);
			break;
		case 2:
			ContactDel(&con);
			break;
		case 3:
			ContactModify(&con);
			break;
		case 4:
			ContactFind(&con);
			break;
		case 5:
			ContactShow(&con);
			break;
		case 0:
			printf("goodbye~\n");
			break;
		default:
			printf("您的输入有误，请重新输入\n");
			break;
		}
	} while (op != 0);
	ContactDestroy(&con);
	return 0;
}