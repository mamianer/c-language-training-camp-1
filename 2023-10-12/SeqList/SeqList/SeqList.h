#pragma once
#include"Contact.h"
#include<stdio.h>
#include<stdlib.h>
#include<assert.h>
#include<stdbool.h>

//动态顺序表
//typedef char SLDataType;
// 
//更改数据类型为通讯录数据类型
//以下的方式任意选择即可
typedef struct ContactInfo SLDataType;
//typedef CInfo SLDataType;

typedef struct SeqList
{
	SLDataType* a;
	int size;  //顺序表中有效的数据个数
	int capacity;  //顺序表当前的空间大小
}SL;
//typedef struct SeqList SL;

//对顺序表进行初始化
void SLInit(SL* ps);
void SLDestroy(SL* ps);

//头部/尾部 插入/删除
void SLPushBack(SL* ps, SLDataType x);
void SLPushFront(SL* ps, SLDataType x);
void SLPopBack(SL* ps);
void SLPopFront(SL* ps);

void SLPrint(SL* ps);
bool SLIsEmpty(SL* ps);

//在任意位置插入删除
//在指定的位置之前插入数据
void SLInsert(SL* ps, int pos, SLDataType x);
//删除指定位置的数据
void SLErase(SL* ps, int pos);

bool SLFind(SL* ps, SLDataType x);