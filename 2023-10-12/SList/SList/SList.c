#include"SList.h"

void SLPrint(SLNode* phead) {
	//循环打印
	//可以用phead直接来访问，但是注意，当代码走到
	//第14行的时候，此时phead已经变成NULL了
	//若代码没写完，我还要继续使用指向第一个节点的地址时，这时我就
	//找不到第一个节点的地址
	SLNode* pcur = phead;
	while (pcur)
	{
		printf("%d ->", pcur->data);
		pcur = pcur->next;
	}
	printf("NULL\n");
}
SLNode* SLBuyNode(SLDataType x) {
	SLNode* node = (SLNode*)malloc(sizeof(SLNode));
	node->data = x;
	node->next = NULL;
	return node;
}
//尾插
void SLPushBack(SLNode** pphead, SLDataType x) {
	SLNode* node = SLBuyNode(x);
	if (*pphead == NULL) {
		*pphead = node;
		return;
	}
	//说明链表不为空,找尾
	SLNode* pcur = *pphead;
	while (pcur->next)
	{
		pcur = pcur->next;
	}
	pcur->next = node;
}